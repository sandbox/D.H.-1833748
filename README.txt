// $Id$

This module to supplies:
- a content type based on the google maps tools module (http://drupal.org/project/gmaps)
- a block to display the latest traveled locations
- a block which draws a map with all traveled locations connected by polylines and a sortable list besides

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

No special configuration needed.

-- Author --

David Hoffmann
david.hoffmann@easycheck.net
