<?php

/**
 * @file
 * Page callback file for the travelmap module.
 */

/**
 * Menu callback; 
 * displays a Drupal page containing recent travelmap entries of a given user.
 */
function travelmap_page_user($account) {
  global $user;

  drupal_set_title($title = t("@name's travelmap", array('@name' => $account->name)));

  $items = array();

  if (($account->uid == $user->uid) && user_access('create travelmap entries')) {
    $items[] = l(t('Post new travelmap entry.'), "node/add/travelmap");
  }
  elseif ($account->uid == $user->uid) {
    $items[] = t('You are not allowed to post a new travelmap entry.');
  }

  $output = theme('item_list', $items);

  $result = pager_query(db_rewrite_sql("SELECT n.nid, n.sticky, n.created FROM {node} n WHERE n.type = 'travelmap' AND n.uid = %d AND n.status = 1 ORDER BY n.sticky DESC, n.created DESC"), variable_get('default_nodes_main', 10), 0, NULL, $account->uid);
  $has_posts = FALSE;
  
  while ($node = db_fetch_object($result)) {
    $output .= node_view(node_load($node->nid), 1);
    $has_posts = TRUE;
  }
  
  if ($has_posts) {
    $output .= theme('pager', NULL, variable_get('default_nodes_main', 10));
  }
  else {
    if ($account->uid == $user->uid) {
      drupal_set_message(t('You have not created any travelmap entries.'));
    }
    else {
      drupal_set_message(t('!author has not created any travelmap entries.', array('!author' => theme('username', $account))));
    }
  }

  return $output;
}

/**
 * Menu callback;
 * displays a Drupal page containing recent travelmap entries of all users.
 */
function travelmap_page_last() {
  global $user;

  $output = '';
  $items = array();

  if (user_access('create travelmap entries')) {
    $items[] = l(t('Create new travelmap entry.'), "node/add/travelmap");
  }

  $output = theme('item_list', $items);

  $result = pager_query(db_rewrite_sql("SELECT n.nid, n.sticky, n.created FROM {node} n WHERE n.type = 'travelmap' AND n.status = 1 ORDER BY n.sticky DESC, n.created DESC"), variable_get('default_nodes_main', 10));
  $has_posts = FALSE;

  while ($node = db_fetch_object($result)) {
    $output .= node_view(node_load($node->nid), 1);
    $has_posts = TRUE;
  }
  
  if ($has_posts) {
    $output .= theme('pager', NULL, variable_get('default_nodes_main', 10));
  }
  else {
    drupal_set_message(t('No travelmap entries have been created.'));
  }

  return $output;
}
