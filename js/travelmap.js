
















































// $Id:
if (Drupal.jsEnabled) {

  //initialize the map  
  Drupal.behaviors.travelmap = function(context) {
    $('.travelmap:not(.travelmap-processed)', context).addClass('travelmap-processed').each( function(){
      $.each(Drupal.settings.travelmap.maps, function(key, value) { 
        drawMap(value, context);
      });
      return false;
    });
	}
  
	var map;

	function drawMap(data, context) {
	  if (GBrowserIsCompatible() && (typeof data.general != 'undefined')) {
      map = new GMap2(document.getElementById(data.general.map_title));
      var markerIcon = new GIcon();
      var markerPath = data.general.marker_path;
      markerIcon.image = markerPath + '/marker.png';
      //markerIcon.shadow = markerPath + '/shadow.png';
      markerIcon.iconSize = new GSize(32,37);
      markerIcon.shadowSize = new GSize(32,37);
      markerIcon.iconAnchor = new GPoint(16,37);
      markerIcon.infoWindowAnchor = new GPoint(16,0);
      markerIcon.printImage = markerPath + '/printImage.gif';
      markerIcon.mozPrintImage = markerPath + '/mozPrintImage.gif';
      //markerIcon.printShadow = markerPath + '/printShadow.png';
      markerIcon.transparent = markerPath + '/transparent.png';
      markerIcon.imageMap = [29,0,30,1,31,2,31,3,31,4,31,5,31,6,31,7,31,8,31,9,31,10,31,11,31,12,31,13,31,14,31,15,31,16,31,17,31,18,31,19,31,20,31,21,31,22,31,23,31,24,31,25,31,26,31,27,31,28,31,29,30,30,29,31,23,32,22,33,21,34,20,35,19,36,12,36,11,35,10,34,9,33,8,32,2,31,1,30,0,29,0,28,0,27,0,26,0,25,0,24,0,23,0,22,0,21,0,20,0,19,0,18,0,17,0,16,0,15,0,14,0,13,0,12,0,11,0,10,0,9,0,8,0,7,0,6,0,5,0,4,0,3,0,2,1,1,2,0];

      var bounds = new GLatLngBounds();
      var markers = [];
      var zIndexes = []; 
      //if results draw markers
      if (typeof data.coordinates != 'undefined') {
        //draw all markers
        for (var i = 0; i < data.coordinates.length; i++) {
          point = new GLatLng(parseFloat(data.coordinates[i]['latitude']),parseFloat(data.coordinates[i]['longitude']));
          marker = new GMarker(point,markerIcon);
          map.addOverlay(marker);
          var label = new ELabel(point, '<div class="travelmap_marker" id="marker-' + data.general.map_title + '-' + i + '"><div class="travelmap_marker_content">' + (i + 1) + '</div></div><div style="clear:both;"></div><div class="travelmap_marker_title" id="travelmap_marker_title_' + i + '"><span>' + data.coordinates[i]['title'] + '</span></div>' + '<div style="clear:both;"></div><div id="travelmap_node_content_' + i + '" class="travelmap_node_content"><span id="travelmap_node_content_close_' + i + '" class="travelmap_node_content_close">X</span>' + data.coordinates[i]['content'] + '</div>', "travelmap_marker_wrapper", 0, 0, true);
          map.addOverlay(label);
          bounds.extend(marker.getPoint());
          markers[i] = marker;
          var zIndex = parseInt(Number($('#marker-'+ data.general.map_title + '-' + i).parent().parent().css('z-index')));
          zIndexes[i] = zIndex;
          if (i > 0 && data.coordinates.length > 1) {
            switch (data.general.polyline_type) {
              //straight
              case '1':
                var polyOptions = {};
                var polyline = new GPolyline([
                new GLatLng(parseFloat(data.coordinates[i-1]['latitude']),parseFloat(data.coordinates[i-1]['longitude'])),
                new GLatLng(parseFloat(data.coordinates[i]['latitude']), parseFloat(data.coordinates[i]['longitude']))
                ], data.general.polyline_color, parseFloat(data.general.polyline_width), parseFloat(data.general.polyline_opacity), polyOptions);
                map.addOverlay(polyline);
                break;
              // curved
              case '2':
                $('#' + data.general.map_title).curvedLine({
                  LatStart: parseFloat(data.coordinates[i-1]['latitude']), 
                  LngStart: parseFloat(data.coordinates[i-1]['longitude']), 
                  LatEnd: parseFloat(data.coordinates[i]['latitude']), 
                  LngEnd: parseFloat(data.coordinates[i]['longitude']),
                  Color: data.general.polyline_color,
                  Weight: parseFloat(data.general.polyline_width),
                  Opacity: parseFloat(data.general.polyline_opacity)
                });
                break;
              // geodesic
              case '0':
              default:
                var polyOptions = {geodesic:true};
                var polyline = new GPolyline([
                new GLatLng(parseFloat(data.coordinates[i-1]['latitude']),parseFloat(data.coordinates[i-1]['longitude'])),
                new GLatLng(parseFloat(data.coordinates[i]['latitude']), parseFloat(data.coordinates[i]['longitude']))
                ], data.general.polyline_color, parseFloat(data.general.polyline_width), parseFloat(data.general.polyline_opacity, polyOptions));
                map.addOverlay(polyline);
                break;
            }
          }
        }
        
        if (data.general.route_is_closed == '1' && data.coordinates.length > 1) {
          switch (data.general.polyline_type) {
            //straight
            case '1':
              var polyOptions = {};
              var polyline = new GPolyline([
              new GLatLng(parseFloat(data.coordinates[0]['latitude']),parseFloat(data.coordinates[0]['longitude'])),
              new GLatLng(parseFloat(data.coordinates[data.coordinates.length - 1]['latitude']), parseFloat(data.coordinates[data.coordinates.length - 1]['longitude']))
              ], data.general.polyline_color, parseFloat(data.general.polyline_width), parseFloat(data.general.polyline_opacity), polyOptions);
              map.addOverlay(polyline);
              break;
            // curved
            case '2':
              $('#' + data.general.map_title).curvedLine({
                LatStart: parseFloat(data.coordinates[0]['latitude']), 
                LngStart: parseFloat(data.coordinates[0]['longitude']), 
                LatEnd: parseFloat(data.coordinates[data.coordinates.length - 1]['latitude']), 
                LngEnd: parseFloat(data.coordinates[data.coordinates.length - 1]['longitude']),
                Color: data.general.polyline_color,
                Weight: parseFloat(data.general.polyline_width),
                Opacity: parseFloat(data.general.polyline_opacity)
              });
              break;
            // geodesic
            case '0':
            default:
              var polyOptions = {geodesic:true};
              var polyline = new GPolyline([
              new GLatLng(parseFloat(data.coordinates[0]['latitude']),parseFloat(data.coordinates[0]['longitude'])),
              new GLatLng(parseFloat(data.coordinates[data.coordinates.length - 1]['latitude']), parseFloat(data.coordinates[data.coordinates.length - 1]['longitude']))
              ], data.general.polyline_color, parseFloat(data.general.polyline_width), parseFloat(data.general.polyline_opacity, polyOptions));
              map.addOverlay(polyline);
              break;
          }
        }
        
        $('.travelmap_marker').css('background-image', 'url("' + data.general.elabel_marker_image + '")');
        
        $(markers).each(function(i,marker){
          GEvent.addListener(marker, 'mouseover', function() {
            $('#travelmap_marker_title_' + i + '').addClass("travelmap_marker_title_active");
            $('#marker-'+ data.general.map_title + '-' + i).parent().parent().css("z-Index", 9999999999);
          });
          //move the active marker to its original position
          GEvent.addListener(marker, 'mouseout', function() {
            $('#travelmap_marker_title_' + i + '').removeClass("travelmap_marker_title_active");
            if(!$('#travelmap_node_content_' + i + '').hasClass("travelmap_node_content_active")) {
              $('#marker-'+ data.general.map_title + '-' + i).parent().parent().css("z-Index", zIndexes[i]);
            }
          });
          //bind on click listener to the marker and change the first item beside the map
          GEvent.addListener(marker, 'click', function() {
            map.panTo(marker.getLatLng());
            $('.travelmap_node_content').removeClass('travelmap_node_content_active');
            $('#travelmap_node_content_' + i + '').addClass("travelmap_node_content_active");
            $(zIndexes).each(function(j){
              if (i != j) {
                $('#marker-'+ data.general.map_title + '-' + j).parent().parent().css("z-Index", zIndexes[i]);
              }
            });
          });
          $('#travelmap_node_content_close_' + i).click(function() {
            $('.travelmap_node_content').removeClass('travelmap_node_content_active');
          });
        });
        $('.travelmap_title:not(.travelmap_title-processed)', context).addClass('travelmap_title-processed').each( function(i){
          $(this).click(function() {
            map.panTo(markers[i].getLatLng());
            $('.travelmap_node_content').removeClass('travelmap_node_content_active');
            $('#travelmap_node_content_' + i + '').addClass("travelmap_node_content_active");
            $(zIndexes).each(function(j){
              if (i != j) {
                $('#marker-'+ data.general.map_title + '-' + j).parent().parent().css("z-Index", zIndexes[i]);
              }
            });
          });
          $(this).mouseover(function() {
            $('#travelmap_marker_title_' + i + '').addClass("travelmap_marker_title_active");
            $('#marker-'+ data.general.map_title + '-' + i).parent().parent().css("z-Index", 9999999999);
          });
          $(this).mouseout(function() {
            $('#travelmap_marker_title_' + i + '').removeClass("travelmap_marker_title_active");
            if(!$('#travelmap_node_content_' + i + '').hasClass("travelmap_node_content_active")) {
              $('#marker-'+ data.general.map_title + '-' + i).parent().parent().css("z-Index", zIndexes[i]);
            }
          });
        });
      }
      map.getBoundsZoomLevel(bounds);
      map.setCenter(bounds.getCenter(),map.getBoundsZoomLevel(bounds));
      
       // this is a background for the controls
      var travelmap_control_background = document.createElement("div");
      $(travelmap_control_background).addClass('travelmap-control-background');
      map.addControl(new travelmap_control(travelmap_control_background,new Array('travelmap-control-background')), new GControlPosition(G_ANCHOR_TOP_LEFT, new GSize(20, 20)));
      
      //overlay
      //up
      map.addControl(new travelmap_control('',new Array('travelmap-control','travelmap-control-up'),'click','panDirection',new Array(0,1)), new GControlPosition(G_ANCHOR_TOP_LEFT, new GSize(30, 20)));
      //down
      map.addControl(new travelmap_control('',new Array('travelmap-control','travelmap-control-down'),'click','panDirection',new Array(0,-1)), new GControlPosition(G_ANCHOR_TOP_LEFT, new GSize(30, 45)));
      //left
      map.addControl(new travelmap_control('',new Array('travelmap-control','travelmap-control-left'),'click','panDirection',new Array(1,0)), new GControlPosition(G_ANCHOR_TOP_LEFT, new GSize(20, 30)));
      //right
      map.addControl(new travelmap_control('',new Array('travelmap-control','travelmap-control-right'),'click','panDirection',new Array(-1,0)), new GControlPosition(G_ANCHOR_TOP_LEFT, new GSize(46, 29)));
      //zoom in
      map.addControl(new travelmap_control('',new Array('travelmap-control-control','travelmap-control-zoomin'),'click','zoomIn'), new GControlPosition(G_ANCHOR_TOP_LEFT, new GSize(29, 70)));
      //zoom out
      map.addControl(new travelmap_control('',new Array('travelmap-control','travelmap-control-zoomout'),'click','zoomOut'), new GControlPosition(G_ANCHOR_TOP_LEFT, new GSize(29, 100)));

	  }
	}
}
