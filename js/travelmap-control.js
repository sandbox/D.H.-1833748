/**
 * a more generic way to add gmaps controls
 * html and style are required
 * listener, event and event_args are optional
 */  
function travelmap_control(html, style, listener, event, event_args) {
  this.html = html;
  this.style = style;
  this.listener = listener;
  this.event = event;
  this.event_args = event_args;
}

travelmap_control.prototype = new GControl();
travelmap_control.prototype.initialize = function(map) {
  this.travelmap_div = document.createElement("div");
  for (var i = 0; i < this.style.length; i++) {
    $(this.travelmap_div).addClass(this.style[i]);
  }
  if (this.html != '') {
    $(this.travelmap_div).append(this.html);
  }
  if (typeof this.listener != 'undefined') {
    var event = this.event;
    var event_args = this.event_args;
    GEvent.addDomListener(this.travelmap_div, this.listener, function() { 
      switch (event) {
        case 'panDirection':
          map[event](event_args[0],event_args[1]);
          break;
        case 'zoomIn':
        case 'zoomOut':
          map[event]();
          break;
      }
    });
  }
  map.getContainer().appendChild(this.travelmap_div);
  return this.travelmap_div;
}
travelmap_control.prototype.getDefaultPosition = function() { return new GControlPosition(); }

// found via https://groups.google.com/group/Google-Maps-API/msg/d8ba590e37d7177c
GLatLngBounds.prototype.GetMidpoint = function(map) { 
  var proj = map.getCurrentMapType().getProjection(); 
  var zoom = map.getCurrentMapType().getMaximumResolution(); 
  var p1=proj.fromLatLngToPixel(this.getNorthEast(),zoom); 
  var p2=proj.fromLatLngToPixel(this.getSouthWest(),zoom); 
  var p = new GPoint( (p1.x+p2.x)/2, (p1.y+p2.y)/2); 
  return proj.fromPixelToLatLng(p,zoom); 
} 
