<?php

/**
 * @file
 * Page callback file for the travelmap admin settings.
 */

/**
 * Menu callback; displays settings page for the travelmap module.
 */
function travelmap_settings_page() {
  global $user;

  drupal_set_title(t("Travelmap settings"));

  $output = drupal_get_form('travelmap_settings_form');

  return $output;
}


/**
 * Implementation of hook_form().
 */
function travelmap_settings_form() {
  $form['map_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Map settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['map_settings']['marker_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Marker settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['map_settings']['polyline_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Polyline settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['map_settings']['marker_settings']['travelmap_setting_route_is_closed'] = array(
    '#type' => 'checkbox',
    '#title' => t('Connect the first and last marker by a polyline.'),
    '#default_value' => variable_get('travelmap_setting_route_is_closed', 0),
  );
  $form['map_settings']['marker_settings']['travelmap_setting_marker_path'] = array(
    '#type' => 'textfield',
    '#title' => t('The path to the marker images used for the travelmap.'),
    '#default_value' => variable_get('travelmap_setting_marker_path', drupal_get_path('module', 'travelmap') . '/images/marker'),
  );
  $form['map_settings']['marker_settings']['travelmap_setting_elabel_marker_image'] = array(
    '#type' => 'textfield',
    '#title' => t('The image to use for the elabel.'),
    '#default_value' => variable_get('travelmap_setting_elabel_marker_image', drupal_get_path('module', 'travelmap') . '/images/marker/travelmap_marker.png'),
  );
  $form['map_settings']['polyline_settings']['travelmap_setting_polyline_type'] = array(
    '#type' => 'radios',
    '#title' => t('Polyline type (curved, straight or geodesic)'),
    '#default_value' => variable_get('travelmap_setting_polyline_type', 0),
    '#options' => array(t('Geodesic'), t('Straight'), t('Curved')),
  );
  $form['map_settings']['polyline_settings']['travelmap_setting_polyline_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Polyline color (without leading #)'),
    '#default_value' => variable_get('travelmap_setting_polyline_color', '#0068B3'),
  );
  $form['map_settings']['polyline_settings']['travelmap_setting_polyline_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Polyline width'),
    '#default_value' => variable_get('travelmap_setting_polyline_width', 2),
  );
  $form['map_settings']['polyline_settings']['travelmap_setting_polyline_opacity'] = array(
    '#type' => 'textfield',
    '#title' => t('Polyline opacity'),
    '#default_value' => variable_get('travelmap_setting_polyline_opacity', 1),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
 
  return $form;
}

/**
 * Implementation of hook_form_submit().
 */
function travelmap_settings_form_submit($form, &$form_state) {
  $travelmap_items = array();
  foreach ($form_state['values'] as $key => $value) {
    if (strpos($key, 'travelmap_') !== FALSE) {
      drupal_set_message($value);
      variable_set($key, $value);
    }
  }
}